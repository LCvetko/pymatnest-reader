#!/usr/bin/env julia
# specifying packages to use
using Plots;
using DelimitedFiles;

include("input.jl")
# checking to see if the input variables are defined, and bringing them in.
if isdefined(Input,:in_prefix)
	const in_prefix = Input.in_prefix
else
	print("You need to define in_prefix in the input file!")
end
if isdefined(Input,:out_prefix)
	const out_prefix = Input.out_prefix
else
	const out_prefix = in_prefix
end
if isdefined(Input,:analyse_file)
	const analyse_file = Input.analyse_file
else
	const analyse_file = in_prefix * ".analyse"
end
if isdefined(Input,:num_processors)
	const num_processors = Input.num_processors
else
	const num_processors = 1
end
if isdefined(Input,:traj_num)
	const traj_num = Input.traj_num
else
	const traj_num = rand(0:(num_processors-1))
end




#getting the .traj and .analyse files
traj_file = string(in_prefix, ".traj.", traj_num, ".extxyz")
traj_data = readdlm(traj_file)
analyse_data = readdlm(analyse_file, skipstart = 2)

temps = analyse_data[:,1]
potentials = analyse_data[:,3]
capacities = analyse_data[:,4]

# getting the peaks from the heat capacity curve
function GetPeaks(dataset)
	# first finding the number of peaks
	n = 0
	for i in 2:length(dataset)-1
		if dataset[i] >= dataset[i-1] && dataset[i] >= dataset[i+1]
			n += 1
		end
	end
	println("There are ", n, " peaks.")

	# getting the locations of the peaks (meaning the indeces, not actual temperatures yet)
	peaks = fill(0,n)
	m=1
	for i in 2:length(dataset)-1
		if dataset[i] >= dataset[i-1] && dataset[i] >= dataset[i+1]
			peaks[m] = i
			m += 1
		end
	end
	return peaks
end

peaks = GetPeaks(capacities)#, xlim=(0,xdim), ylim=(0,ydim), zlim=(0,zdim)

# defining a function to return peak values
tempPeaks = temps[peaks]
potPeaks = potentials[peaks]
capPeaks = capacities[peaks]

# getting ready to read the important info from the trajectory files
nparts = traj_data[1,1];
nlines = nparts + 2;
ncells = Int64(size(traj_data)[1]/nlines)
println(ncells)

# defining a function that finds the cell configuration with the closes energy to a given potential
function PlotCellConfig(file, potential, nparts, nlines, ncells)
	peind = 11
	optimalindex = 1
	for i in 2:ncells
		index = i*nlines+1
		if abs(parse(Float64, file[(i-1)*nlines+2,peind][11:end]) - potential) < abs(parse(Float64, file[(optimalindex-1)*nlines+2,peind][11:end]) - potential)
			optimalindex = i
		end
	end

	# getting the numbers of each type of atom
	np1=0;
	sp1="null";
	np2=0;
	sp2="null";
	np3=0;
	sp3="null";
	for i in 3:nparts+2
	        if sp1 == "null" || file[i,1] == sp1
	                sp1 = file[i,1]
	                np1 += 1;
	        elseif sp2 == "null" || file[i,1] == sp2
	                sp2 = file[i,1]
	                np2 += 1;
	        elseif sp3 == "null" || file[i,1] == sp3
	                sp3 = file[i,1]
	                np3 += 1
	        end
	end
	println(sp1, ": ", np1, " atoms.")
	println(sp2, ": ", np2, " atoms.")
	println(sp3, ": ", np3, " atoms.")

	# getting ready to record all the postions
	p1xpos=fill(0.0,np1,ncells)
	p2xpos=fill(0.0,np2,ncells)
	p3xpos=fill(0.0,np3,ncells)
	p1ypos=fill(0.0,np1,ncells)
	p2ypos=fill(0.0,np2,ncells)
	p3ypos=fill(0.0,np3,ncells)
	p1zpos=fill(0.0,np1,ncells)
	p2zpos=fill(0.0,np2,ncells)
	p3zpos=fill(0.0,np3,ncells)
	label=fill("",ncells)


	for i in 1:ncells
        	#println((i-1)*nlines+2)
        	label[i]=file[(i-1)*nlines+2,11]
        	for j in 1:np1
        	        println(file[(i-1)*nparts+j+2,2])
        	        p1xpos[j,i]=file[(i-1)*nlines+j+2,2]
        	        p1ypos[j,i]=file[(i-1)*nlines+j+2,3]
        	        p1zpos[j,i]=file[(i-1)*nlines+j+2,4]
        	end
        	for j in 1:np2
        	        p2xpos[j,i]=file[(i-1)*nlines+np1+j+2,2]
        	        p2ypos[j,i]=file[(i-1)*nlines+np1+j+2,3]
        	        p2zpos[j,i]=file[(i-1)*nlines+np1+j+2,4]
        	end
        	for j in 1:np3
        	        p3xpos[j,i]=file[(i-1)*nlines+np1+np2+j+2,2]
        	        p3ypos[j,i]=file[(i-1)*nlines+np1+np2+j+2,3]
        	        p3zpos[j,i]=file[(i-1)*nlines+np1+np2+j+2,4]
		end
	end
	# getting the (rough) dimmensions of the cell
	xdim = parse(Float64, file[2,1][10:end])
	ydim = file[2,5]
	zdim = parse(Float64, file[2,9][1:end-1])

	returnplot = scatter3d(p1xpos[:,optimalindex], p1ypos[:,optimalindex], p1zpos[:,optimalindex], label=sp1, size=(1000,1000),title=label[optimalindex], xlim=(0,xdim), ylim=(0,ydim), zlim=(0,zdim));
	scatter3d!(p2xpos[:,optimalindex],p2ypos[:,optimalindex],p2zpos[:,optimalindex],label=sp2);
	scatter3d!(p3xpos[:,optimalindex],p3ypos[:,optimalindex],p3zpos[:,optimalindex],label=sp3);
	return returnplot
end
# making a loop that calls PlotCellConfig for each of the peaks (file, potential, nparts, nlines, ncells)
for i in 1:length(peaks)
	peakplot = PlotCellConfig(traj_data, potPeaks[i], nparts, nlines, ncells)
	savefig(peakplot, string(out_prefix, "_peak-config_", i, ".png"))
end
