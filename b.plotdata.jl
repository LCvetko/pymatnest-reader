#!/usr/bin/env julia
using Plots;
using DelimitedFiles

include("input.jl")
# checking to see if the input variables are defined, and bringing them in.
if isdefined(Input,:in_prefix)
	const in_prefix = Input.in_prefix
else
	print("You need to define in_prefix in the input file!")
end
if isdefined(Input,:out_prefix)
	const out_prefix = Input.out_prefix
else
	const out_prefix = in_prefix
end
if isdefined(Input,:analyse_file)
	const analyse_file = Input.analyse_file
else
	const analyse_file = in_prefix * ".analyse"
end
if isdefined(Input,:num_processors)
	const num_processors = Input.num_processors
else
	const num_processors = 1
end
if isdefined(Input,:traj_num)
	const traj_num = Input.traj_num
else
	const traj_num = rand(0:(num_processors-1))
end



_4pdata1 = readdlm(analyse_file, skipstart=2);
temps = _4pdata1[:,1];
_4pcps1 = _4pdata1[:,4];
_energy = _4pdata1[:,3];
numlevels=size(temps)[1];
numused=Int64(round(numlevels*0.8));
font = Plots.font("DejaVu Sans", 24)
#myfonts = Dict(:guidefont=>font, :xtickfont=>font, :ytickfont=>font, :legendfont=>font)
pyplot(guidefont=font, xtickfont=font, ytickfont=font, legendfont=font)
plot(temps, _4pcps1, size=(1000,1000), label="Heat Capacity");
xlabel!("Temperature (K)")
savefig(out_prefix * "_cv.png")
#plot(temps[numlevels-numused:numlevels], _energy[numlevels-numused:numlevels], size=(1000,1000), label="Potential Energy for ZrH MTP, 16 Zr 32 H");
plot(temps, _energy, size=(1000,1000), label="Potential Energy");
xlabel!("Temperature (K)")
savefig(out_prefix * "_u.png")
