#!/usr/bin/env julia
# specifying packages to use
using Plots;
using DelimitedFiles;

include("input.jl")
# checking to see if the input variables are defined, and bringing them in.
if isdefined(Input,:in_prefix)
	const in_prefix = Input.in_prefix
else
	print("You need to define in_prefix in the input file!")
end
if isdefined(Input,:out_prefix)
	const out_prefix = Input.out_prefix
else
	const out_prefix = in_prefix
end
if isdefined(Input,:analyse_file)
	const analyse_file = Input.analyse_file
else
	const analyse_file = in_prefix * ".analyse"
end
if isdefined(Input,:num_processors)
	const num_processors = Input.num_processors
else
	const num_processors = 1
end
if isdefined(Input,:traj_num)
	const traj_num = Input.traj_num
else
	const traj_num = rand(0:(num_processors-1))
end

data = readdlm(analyse_file, skipstart = 2)
temps = data[:,1]
cps = data[:,4]
font = Plots.font("DejaVu Sans", 24)
#myfonts = Dict(:guidefont=>font, :xtickfont=>font, :ytickfont=>font, :legendfont=>font)
pyplot(guidefont=font, xtickfont=font, ytickfont=font, legendfont=font)
plot(temps, cps, size=(1000,1000), label="");
title!("Heat Capacity")
savefig(out_prefix * "_cv.png")
