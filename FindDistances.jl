#!/usr/bin/env julia
# specifying packages to use
using Plots;
using DelimitedFiles;

include("input.jl")
# checking to see if the input variables are defined, and bringing them in.
if isdefined(Input,:in_prefix)
	const in_prefix = Input.in_prefix
else
	print("You need to define in_prefix in the input file!")
end
if isdefined(Input,:out_prefix)
	const out_prefix = Input.out_prefix
else
	const out_prefix = in_prefix
end
if isdefined(Input,:analyse_file)
	const analyse_file = Input.analyse_file
else
	const analyse_file = in_prefix * ".analyse"
end
if isdefined(Input,:num_processors)
	const num_processors = Input.num_processors
else
	const num_processors = 1
end
if isdefined(Input,:traj_num)
	const traj_num = Input.traj_num
else
	const traj_num = rand(0:(num_processors-1))
end

traj_file = string(in_prefix, ".traj.", traj_num, ".extxyz")

#getting the .traj file
traj_data = readdlm(traj_file)

# getting ready to read the important info from the trajectory files
nparts = traj_data[1,1];
nlines = nparts + 2;
ncells = Int64(size(traj_data)[1]/nlines)

function Distance(x1, y1, z1, x2, y2, z2)
	distance = sqrt((x2-x1)^2 + (y2-y1)^2 + (z2-z1)^2)
	return distance
end

# getting all the distances between a set of points
function GetDists(label, xvals, yvals, zvals)
	n = size(xvals)[1] - 1
	numdists = Int64(1/2*(n^2 + n))
	dists = fill(0.0, numdists)
	pairs = [];
	distindex = 1
	mindist = 100
	for i in 1:(n+1)
		for j in i+1:(n+1)
			dists[distindex] = Distance(xvals[i], yvals[i], zvals[i], xvals[j], yvals[j], zvals[j])
			push!(pairs,label[i] * "-" * label[j])
			distindex += 1
		end
	end
	return dists, pairs
end

function GetCoords(file, nparts, nlines, ncells)
	xpos = fill(0.0, ncells, nparts)
	ypos = fill(0.0, ncells, nparts)
	zpos = fill(0.0, ncells, nparts)
	label = fill("", ncells, nparts)
	for i in 1:ncells
		ind = (i-1)*nlines+3
		label[i,:] = file[ind:(ind+nparts-1),1]
		xpos[i,:] = file[ind:(ind+nparts-1),2]
		ypos[i,:] = file[ind:(ind+nparts-1),3]
		zpos[i,:] = file[ind:(ind+nparts-1),4]
	end
	return label, xpos, ypos, zpos
end


label, xpos, ypos, zpos = GetCoords(traj_data, nparts, nlines, ncells)

ndists = Int64(1/2*((nparts-1)^2 + (nparts-1)))
distances = fill(0.0,ncells,ndists)
pairs = fill("",ncells,ndists)
for i in 1:ncells
	distances[i,:], pairs[i,:] = GetDists(label[i,:],xpos[i,:],ypos[i,:],zpos[i,:])
end

maxdist = maximum(distances)

distsplot = scatter(distances[1,:], label="distances", size=(1000,1000), ylims=(0,maxdist), markersize=10)
#savefig("distances_new.png")

anim = Animation()

mindist = []
minpairs = []
energies = []
for i in 1:ncells
	#scatter(distances[i,:], label="distances", size=(1000,1000), ylims=(0,maxdist), markersize=10)
	#frame(anim)
	d,ind = findmin(distances[i,:])
	push!(mindist, d)
	push!(minpairs,pairs[i,ind])
	push!(energies, parse(Float64,traj_data[(i-1)*nlines+2,11][11:end]))
end
#gif(anim, out_prefix * "_distances.gif", fps=5)
#print(pairs)
scatter(distances[1000,:], label="distances", size=(1000,1000), ylims=(0,maxdist), markersize=10)
savefig("temp.png")

function GetMessage(distances, ncells, ndists)
	message = []
	threshold = 1
	for i in 1:ncells
		for j in 1:ndists
			if distances[i,j] < threshold
				push!(message, string("Cell number ", i, " contained this distance: ", distances[i,j], "."))
			end
		end
	end
	return message
end
writedlm(out_prefix * "_tooclose.txt", GetMessage(distances,ncells,ndists))
minoutput = []
for i in 1:length(energies)
	push!(minoutput, string("Energy: ", energies[i], ". Minimum distance: ", mindist[i], ". Pair: ", pairs[i]))
end
writedlm(out_prefix * "_mindists.txt", minoutput)
plot(mindist,-energies/nparts,label="",yaxis=:log,ylims=(10^0,10^3))
xlabel!("Distance (Angstroms)")
ylabel!("Energy/Atom (eV)")
#yticks!([1,10,10^2,10^3,10^4,10^5,10^6,10^7,10^8,10^9,10^10,10^11])
savefig(out_prefix * "_mindists.png")
plot(mindist, label="",yaxis=("Minimum Distance (Angstroms)"),xaxis=("Iteration",:log))
savefig(out_prefix * "_dist_it.png")
