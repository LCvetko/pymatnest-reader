#!/usr/bin/env julia
using Plots;
using DelimitedFiles
_4pdata1 = readdlm("file.out");
_4pdata2 = readdlm("dtest4plong2.data");
_4pdata3 = readdlm("dtest4plong3.data");
_4pdata4 = readdlm("dtest4plong4.data");
_7pdata1 = readdlm("dtest7p.data");
_7pdata2 = readdlm("dtest7plong2.data");
_7pdata3 = readdlm("dtest7plong3.data");
_7pdata4 = readdlm("dtest7plong4.data");
_13pdata1 = readdlm("dtest13p.data");
_13pdata2 = readdlm("dtest13plong2.data");
_13pdata3 = readdlm("dtest13plong3.data");
_13pdata4 = readdlm("dtest13plong4.data");
temps = _4pdata1[:,1];
_4pcps1 = _4pdata1[:,4]/4;
_4pcps2 = _4pdata2[:,4]/4;
_4pcps3 = _4pdata3[:,4]/4;
_4pcps4 = _4pdata4[:,4]/4;
_7pcps1 = _7pdata1[:,4]/7;
_7pcps2 = _7pdata2[:,4]/7;
_7pcps3 = _7pdata3[:,4]/7;
_7pcps4 = _7pdata4[:,4]/7;
_13pcps1 = _13pdata1[:,4]/13;
_13pcps2 = _13pdata2[:,4]/13;
_13pcps3 = _13pdata3[:,4]/13;
_13pcps4 = _13pdata4[:,4]/13;
_4pavg = (_4pcps1 + _4pcps2 + _4pcps3 + _4pcps4)/4
_7pavg = (_7pcps1 + _7pcps2 + _7pcps3 + _7pcps4)/4
_13pavg = (_13pcps1 + _13pcps2 + _13pcps3 + _13pcps4)/4
font = Plots.font("DejaVu Sans", 24)
#myfonts = Dict(:guidefont=>font, :xtickfont=>font, :ytickfont=>font, :legendfont=>font)
pyplot(guidefont=font, xtickfont=font, ytickfont=font, legendfont=font)
plot(temps, _4pcps1, size=(1000,1000), label="CV using untrained MTP");
savefig("untrained.png")
#plot(temps, [_4pcps1, _4pcps2, _4pcps3, _4pcps4], size=(1000,1000), label=["4p Long 1" "4p Long 2" "4p Long 3" "4p Long 4"]);
#plot!(temps, cpsb, label="4p Second Run");
#plot!(temps, cpsc, label="4p Long Run");
#savefig("4plong.png")
#plot(temps, [_7pcps1, _7pcps2, _7pcps3, _7pcps4], size=(1000,1000), label=["7p Long 1" "7p Long 2" "7p Long 3" "7p Long 4"]);
#plot!(temps, cps2b, label="7p Second Run");
#plot!(temps, cps2c, label="7p Long Run");
#savefig("7plong.png")
#plot(temps, [_13pcps1, _13pcps2, _13pcps3, _13pcps4], size=(1000,1000), label=["13p Long 1" "13p Long 2" "13p Long 3" "13p Long 4"]);
#plot!(temps, cps3b, label="13p Second Run");
#plot!(temps, cps3c, label="13p Long Run");
#savefig("13plong.png")
#plot(temps, [_4pavg, _7pavg, _13pavg], size=(1000,1000), label=["4 Particles" "7 Particles" "13 Particles"]);
#savefig("avgcps.png")
