#!/usr/bin/env julia
# specifying packages to use
using Plots;
using DelimitedFiles;
gr(markerstrokewidth=0.3,markersize=10)
include("input.jl")
# checking to see if the input variables are defined, and bringing them in.
if isdefined(Input,:in_prefix)
	const in_prefix = Input.in_prefix
else
	print("You need to define in_prefix in the input file!")
end
if isdefined(Input,:out_prefix)
	const out_prefix = Input.out_prefix
else
	const out_prefix = in_prefix
end
if isdefined(Input,:analyse_file)
	const analyse_file = Input.analyse_file
else
	const analyse_file = in_prefix * ".analyse"
end
if isdefined(Input,:num_processors)
	const num_processors = Input.num_processors
else
	const num_processors = 1
end
if isdefined(Input,:traj_num)
	const traj_num = Input.traj_num
else
	const traj_num = rand(0:(num_processors-1))
end

#getting the .traj file
traj_file = string(in_prefix, ".traj.", traj_num, ".extxyz")

# reading in the traj file
file=readdlm(traj_file);
# nparts is the number of particles
nparts=file[1,1];
# ncells is the number of cells. For each cell there is one line of code per
ncells=Int64(size(file)[1]/(nparts+2))
# getting the numbers of each type of atom
function GetAtomTypes(file)
	np1=0;
	sp1="null";
	np2=0;
	sp2="null";
	np3=0;
	sp3="null";
	for i in 3:nparts+2
		if sp1 == "null" || file[i,1] == sp1
			sp1 = file[i,1]
			np1 += 1;
		elseif sp2 == "null" || file[i,1] == sp2
			sp2 = file[i,1]
			np2 += 1;
		elseif sp3 == "null" || file[i,1] == sp3
			sp3 = file[i,1]
			np3 += 1
		end
	end
	println(sp1, ": ", np1, " atoms.")
	println(sp2, ": ", np2, " atoms.")
	println(sp3, ": ", np3, " atoms.")
	return [[np1,np2,np3],[sp1,sp2,sp3]]
end
parts = GetAtomTypes(file)
np1 = parts[1][1]
np2 = parts[1][2]
np3 = parts[1][3]
sp1 = parts[2][1]
sp2 = parts[2][2]
sp3 = parts[2][3]
# recording the position of each particle
p1xpos=fill(0.0,np1,ncells)
p2xpos=fill(0.0,np2,ncells)
p3xpos=fill(0.0,np3,ncells)
p1ypos=fill(0.0,np1,ncells)
p2ypos=fill(0.0,np2,ncells)
p3ypos=fill(0.0,np3,ncells)
p1zpos=fill(0.0,np1,ncells)
p2zpos=fill(0.0,np2,ncells)
p3zpos=fill(0.0,np3,ncells)
label=fill("",ncells);

# Iterate for each atom in a cell
nlines=nparts+2;
for i in 1:ncells
	#println((i-1)*nlines+2)
	label[i]=file[(i-1)*nlines+2,11]
	for j in 1:np1
		#println(file[(i-1)*nparts+j+2,2])
		p1xpos[j,i]=file[(i-1)*nlines+j+2,2]
	            p1ypos[j,i]=file[(i-1)*nlines+j+2,3]
	            p1zpos[j,i]=file[(i-1)*nlines+j+2,4]
	end
	    for j in 1:np2
	            p2xpos[j,i]=file[(i-1)*nlines+np1+j+2,2]
	            p2ypos[j,i]=file[(i-1)*nlines+np1+j+2,3]
	            p2zpos[j,i]=file[(i-1)*nlines+np1+j+2,4]
	    end
	    for j in 1:np3
	            p3xpos[j,i]=file[(i-1)*nlines+np1+np2+j+2,2]
	            p3ypos[j,i]=file[(i-1)*nlines+np1+np2+j+2,3]
	            p3zpos[j,i]=file[(i-1)*nlines+np1+np2+j+2,4]
	    end
end
println(ncells)
#println("sup")
#print(p1zpos[:,20])

# getting the (rough) dimmensions of the cell
xdim = parse(Float64, file[2,1][10:length(file[2,1])])
ydim = file[2,5]
zdim = parse(Float64, file[2,9][1:length(file[2,9])-1])

anim = Animation();
#font = Plots.font("DejaVu Sans", 24)
#pyplot(guidefont=font, xtickfont=font, ytickfont=font, legendfont=font)
println(p1xpos[:,20],p1ypos[:,20],p1zpos[:,20])
scatter3d(p1xpos[:,20], p1ypos[:,20], p1zpos[:,20], label=sp1, size=(1000,1000),title=label[20]);
scatter3d!(p2xpos[:,20],p2ypos[:,20],p2zpos[:,20],label=sp2);
scatter3d!(p3xpos[:,20],p3ypos[:,20],p3zpos[:,20],label=sp3);
savefig(out_prefix * "_traj.png");

for i in 1:1000
	scatter3d(p1xpos[:,i], p1ypos[:,i], p1zpos[:,i], label=sp1, size=(1000,1000),title=label[i], xlim=(0,xdim), ylim=(0,ydim), zlim=(0,zdim));
	scatter3d!(p2xpos[:,i],p2ypos[:,i],p2zpos[:,i],label=sp2);
	scatter3d!(p3xpos[:,i],p3ypos[:,i],p3zpos[:,i],label=sp3);
	if i == 1 || i == 2
		#savefig(string("new_mtp_",i,".png"))
	end
	frame(anim)
end
gif(anim, out_prefix * ".mp4")
