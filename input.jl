#!/usr/bin/env julia
# This is a template for the imput file that the other files in pymatnest-reader use to do their jobs
# This should be the only thing you have to edit to make the programs work for the output you got from pymatnest
# Change the values in here, or get rid of them altogether if you want to use the defaults, but keep the file name as input.jl, and don't change either of the next two lines

module Input
export in_prefix, out_prefix, analyse_file, num_processors, traj_num

###########################################################################################################################################
# These are the basic options for the input file. If you are using multiple processors with pymatnest or you want to compare multiple sets of data, you'll need to check out the advanced options below. Otherwise, just focus on this section here.

# in_prefix: this should include the path to your pymatnest output data and be the same as the out_file_prefix setting you set for the input file you used to run pymatnest
# for example:
#	const in_prefix = "../data/experiment_1/NS_MD_lammps_lennard-jones_1"
const in_prefix = "../pymatnest_data/HfNiTi/debug_1/NS_MTP_HfNiTi_16_16_16_debug_1"

# out_prefix: this should include the path to where you want the plots and animations to go once you run a program in pymatnest-reader. It is not required that prefix for this variable be the same as the one for the previous variable, but it's reccommended to keep track of your data. Default is the same as your in_prefix
# for example:
#	const out_prefix = "../plots/experiment_1/NS_MD_lammps_lennard-jones_1"

# analyse_file: this is the path to and the name of the file you obtained by running ns_analyse on your data. Default is in_prefix * ".analyse"
# for example:
#	const analyse_file = "../data/experiment_1/NS_MD_lammps_lennard-jones_1.analyse"

###########################################################################################################################################
# if you only used one processor to run pymatnest, then you don't need to worry about the settings below here

# num_processors is the number of processors you used to run pymatnest. Unless you told it otherwise, this will be 1. The default is 1
# for example:
#	const num_processors = 16
const num_processors = 10

# traj_num is the number of the *.traj file you want to use to make your plots. Its range is 0 to (num_processors-1). If it's not specified, then a random value in this range will be used
# for example:
#	const traj_num = 4




# This 'end' needs to be here because of the module
end
